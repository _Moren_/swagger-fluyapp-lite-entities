'use strict';

const { PORT } = require('./config');
const express = require('express');
const app = express();
const swaggerUI = require('swagger-ui-express');
const docs = require('./src/docs/index')
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(docs));
/////
app.listen(PORT, () => {
    console.log(`Swagger running on port: ${PORT}`);
});
