require('dotenv').config();
module.exports = {
    PORT: process.env.PORT,
    API_FLUYAPP_LITE_DEV: process.env.API_FLUYAPP_LITE_DEV
}