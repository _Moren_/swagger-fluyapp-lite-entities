
module.exports = {
    tags: [
        {
            name: 'entities'
        },
        {
            name: 'branches'
        },
        {
            name: 'departaments'
        },
        {
            name:'modules'
        },
        {
            name:'services'
        },
        {
            name:'service-branches'
        },
        {
            name:'resason_rests'
        },
        {
            name:'resason_rests_report'
        },
        {
            name:'skills'
        },
        {
            name:'modules_agents'
        },
        {
            name:'modules'
        },
        {
            name:'agent_reasons'
        },
        {
            name:'kiosks'
        },
        {
            name:'themes'
        },
    ]
}