module.exports = {
    createTheme: {
        type: 'object',
        properties: {
            entityId: {
                type: 'number',
                description: 'identificador de una entidad',
                example: 6
            },
            themeName: {
                type: 'string',
                description: 'diversos tipos de colores',
                enum: [
                    "#ff6347",
                    "#3cb371",
                    "#6a5acd",
                ]
            },
            createBy: {
                type: 'number',
                description: 'quién creó la habilidad',
                example: 1
            },
            updateBy: {
                type: 'number',
                description: 'quién actualió la habilidad',
                example: ''
            },
        }
    },
    requestBodyThemeUpdated: {
        type: 'object',
        properties: {
            entityId: {
                type: 'number',
                description: 'identificador de una entidad',
                example: 6
            },
            themeName: {
                type: 'string',
                description: 'diversos tipos de colores',
                enum: [
                    "#ff6347",
                    "#3cb371",
                    "#6a5acd",
                ]
            },
            updateBy: {
                type: 'number',
                description: 'quién actualió la habilidad',
                example: 1
            },
        }
    },
    responseThemeUpdated: {
        type: 'object',
        properties: {
            entityId: {
                type: 'number',
                description: 'identificador de una entidad',
                example: 6
            },
            themeName: {
                type: 'string',
                description: 'diversos tipos de colores',
                enum: [
                    "#ff6347",
                    "#3cb371",
                    "#6a5acd",
                ]
            },
            createBy: {
                type: 'number',
                description: 'quién creó la habilidad',
                example: 1
            },
            updateBy: {
                type: 'number',
                description: 'quién actualió la habilidad',
                example: 1
            },
        }
    },
    responseTheme: {
        type: 'object',
        properties: {
            entityId: {
                type: 'number',
                description: 'identificador de una entidad',
                example: 6
            },
            themeName: {
                type: 'string',
                description: 'diversos tipos de colores',
                enum: [
                    "#ff6347",
                    "#3cb371",
                    "#6a5acd",
                ]
            },
            createBy: {
                type: 'number',
                description: 'quién creó la habilidad',
                example: 1
            },
            updateBy: {
                type: 'number',
                description: 'quién actualió la habilidad',
                example: 1
            },
        }
    }
}