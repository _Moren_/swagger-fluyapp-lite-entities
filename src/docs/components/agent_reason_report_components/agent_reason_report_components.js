module.exports = {
    createAgentReason: {
        type: 'object',
        properties: {
            idReason: {
                type: 'number',
                description: 'idnetificador de la raozn de descanso',
                example: 5
            },
            agentId: {
                type: 'number',
                description: 'identificador del agente',
                example: 7
            },
            moduleId: {
                type: 'number',
                description: 'identificador del modulo de atención',
                example: 7
            },
            workDay: {
                type: 'string',
                description: 'fecha del dia actual de trabajo',
                example: '2021-10-19'
            },

            beginHour: {
                type: 'string',
                description: 'inicio de la hora de descanso',
                example: '2021-10-19 19:10:42.106+00'
            },
            time: {
                type: 'string',
                description: 'fecha del dia actual de trabajo',
                example: 10
            },
            reasonName: {
                type: 'string',
                description: 'describe el tipo de descanso que toma el agente',
                example: 'descanso'
            },
            endHour: {
                type: 'string',
                description: 'hora en la que termina la razon de descanso',
                example: '2021-10-19 19:40:42.106+00'
            },
            branchId: {
                type: 'number',
                description: 'identificador de la sucursal a la que pertenece el agente',
                example: 8
            },
            entityId: {
                type: 'number',
                description: 'identificador de la entidad a la que pertenece el agente',
                example: 7
            },

        }
    },
    requestBodyAgentReasonToFind: {
        type: 'object',
        properties: {
            agentId: {
                type: 'number',
                description: 'identificador del agente',
                example: 7
            },
            branchId: {
                type: 'number',
                description: 'identificador de la sucursal a la que pertenece el agente',
                example: 8
            },
            entityId: {
                type: 'number',
                description: 'identificador de la entidad a la que pertenece el agente',
                example: 7
            },

        }
    },
    createAgentReason: {
        type: 'object',
        properties: {
            idReason: {
                type: 'number',
                description: 'idnetificador de la raozn de descanso',
                example: 5
            },
            agentId: {
                type: 'number',
                description: 'identificador del agente',
                example: 7
            },
            moduleId: {
                type: 'number',
                description: 'identificador del modulo de atención',
                example: 7
            },
            time: {
                type: 'string',
                description: 'fecha del dia actual de trabajo',
                example: 10
            },
            reasonName: {
                type: 'string',
                description: 'describe el tipo de descanso que toma el agente',
                example: 'descanso'
            },
            branchId: {
                type: 'number',
                description: 'identificador de la sucursal a la que pertenece el agente',
                example: 8
            },
            entityId: {
                type: 'number',
                description: 'identificador de la entidad a la que pertenece el agente',
                example: 7
            },

        }
    },
    responseAgentReasonCreated: {
        type: 'object',
        properties: {
            idReason: {
                type: 'number',
                description: 'idnetificador de la raozn de descanso',
                example: 5
            },
            agentId: {
                type: 'number',
                description: 'identificador del agente',
                example: 7
            },
            moduleId: {
                type: 'number',
                description: 'identificador del modulo de atención',
                example: 7
            },
            workDay: {
                type: 'string',
                description: 'fecha del dia actual de trabajo',
                example: '2021-10-19'
            },

            beginHour: {
                type: 'string',
                description: 'inicio de la hora de descanso',
                example: ''
            },
            time: {
                type: 'string',
                description: 'fecha del dia actual de trabajo',
                example: 10
            },
            reasonName: {
                type: 'string',
                description: 'describe el tipo de descanso que toma el agente',
                example: 'descanso'
            },
            endHour: {
                type: 'string',
                description: 'hora en la que termina la razon de descanso',
                example: ''
            },
            branchId: {
                type: 'number',
                description: 'identificador de la sucursal a la que pertenece el agente',
                example: 8
            },
            entityId: {
                type: 'number',
                description: 'identificador de la entidad a la que pertenece el agente',
                example: 7
            },

        }
    }
}