const entities = require('./entities-components/entities-components')
const branches = require('./banches-components/banches-components');
const kioks = require('./kiosks-components/kiosks-components');
const departaments = require('./departaments-components/departaments-components');
const agentReasonReport = require('./agent_reason_report_components/agent_reason_report_components');
const modulesAgents = require('./module-agents-components/module-agents-components');
const modules = require('./modules-components/modules-components');
const ReasonRest = require('./reason-rest-components/reason-rest-components');
const services = require('./services-components/services-components');
const serviceBranches = require('./service-branch-components/service-branch-components');
const skills = require('./skills-comnponents/skills-comnponents');
const themes = require('./themes-componets/themes-componets');


module.exports = {
    components: {
        schemas: {
            ...entities,
            ...branches,
            ...departaments,
            ...agentReasonReport,
            ...kioks,
            ...modulesAgents,
            ...modules,
            ...ReasonRest,
            ...services,
            ...serviceBranches,
            ...skills,
            ...themes

        }
    }
}