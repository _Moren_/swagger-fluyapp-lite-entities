module.exports = {
    createModule: {
        type: 'object',
        properties: {
            numberModule: {
                type: 'number',
                description: 'identificador del módulo',
                example: 106
            },
            entityId: {
                type: 'number',
                description: 'identificador de la entidad',
                example: 7
            },
            branchId: {
                type: 'number',
                description: 'identificador de la sucursal',
                example: 8
            },
            departamentId: {
                type: 'number',
                description: 'identificador de un departamento',
                example: 9
            },

            nameModule: {
                type: 'string',
                description: 'nombre del módulo',
                example: 'Modulo 101'
            },
            createBy: {
                type: 'number',
                description: 'por quien fue creado el módulo',
                example: 1
            },
            updateBy: {
                type: 'number',
                description: 'por quien fue actualizado el módulo',
                example: 9
            },
            status: {
                type: 'boolean',
                description: 'estado del módulo',
                example: 9
            },
        }
    },
    responsebodyModule: {
        type: 'object',
        properties: {
            id: {
                type: 'number',
                description: 'identificador único del módulo',
                example: 1
            },
            numberModule: {
                type: 'number',
                description: 'identificador del módulo',
                example: 106
            },
            nameModule: {
                type: 'string',
                description: 'nombre del módulo',
                example: 'Modulo 101'
            },
        }
    },
    requestBodyUpdateModule: {
        type: 'object',
        properties: {
            numberModule: {
                type: 'number',
                description: 'identificador del módulo',
                example: 106
            },
            nameModule: {
                type: 'string',
                description: 'nombre del módulo',
                example: 'Modulo 101'
            },
            departamentId: {
                type: 'number',
                description: 'identificador de un departamento',
                example: 9
            },
            branchId: {
                type: 'number',
                description: 'identificador de la sucursal',
                example: 8
            },
            updateBy: {
                type: 'number',
                description: 'por quien fue actualizado el módulo',
                example: 9
            },
        }
    }
}
