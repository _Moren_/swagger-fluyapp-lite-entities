module.exports = {
    createReasonRest: {
        type: 'object',
        properties: {
            reasonRestName: {
                type: 'string',
                description: 'nombre descriptivo para el tipo de descanso',
                example: 'Capacitacion'
            },
            entityId: {
                type: 'number',
                description: 'identificador de la entidad asociado a una razón de descanso',
                example: 7
            },
            createBy: {
                type: 'number',
                description: 'por quien fue creado',
                example: 7
            },
            updateBy: {
                type: 'number',
                description: 'por quien fue actualizado',
                example: 8
            },
            status: {
                type: 'boolean',
                description: 'estado de la razón de descanso',
                example: ''
            },
            timeReason: {
                type: 'number',
                description: 'duración de la razón de descanso',
                example: 20
            },
        }
    },
    requestBodyReasonRest: {
        type: 'object',
        properties: {
            reasonRestName: {
                type: 'string',
                description: 'nombre descriptivo para el tipo de descanso',
                example: 'Capacitacion'
            },
            entityId: {
                type: 'number',
                description: 'identificador de la entidad asociado a una razón de descanso',
                example: 7
            },
            createBy: {
                type: 'number',
                description: 'por quien fue creado',
                example: 7
            },
            updateBy: {
                type: 'number',
                description: 'por quien fue actualizado',
                example: 8
            },
            timeReason: {
                type: 'number',
                description: 'duración de la razón de descanso',
                example: 20
            },
        }
    },
    responseReasonRestToUser: {
        type: 'object',
        properties: {
            reasonRestName: {
                type: 'string',
                description: 'nombre descriptivo para el tipo de descanso',
                example: 'Capacitacion'
            },
            id: {
                type: 'number',
                description: 'identificador único de la razón de descanso',
                example: 20
            }
        }
    },
    requestBodyReasonRestUpdate: {
        type: 'object',
        properties: {
            reasonRestName: {
                type: 'string',
                description: 'nombre descriptivo para el tipo de descanso',
                example: 'Capacitacion'
            },
            updateBy: {
                type: 'number',
                description: 'por quien fue actualizado',
                example: 8
            },
            timeReason: {
                type: 'number',
                description: 'duración de la razón de descanso',
                example: 20
            },
        }
    },
    requestBodyStatus: {
        type: 'object',
        properties: {
            status: {
                type: 'boolean',
                description: 'estado de la razón de descanso',
                example: true
            }
        }
    },
    createReasonRestUpdated: {
        type: 'object',
        properties: {
            reasonRestName: {
                type: 'string',
                description: 'nombre descriptivo para el tipo de descanso',
                example: 'Capacitacion'
            },
            entityId: {
                type: 'number',
                description: 'identificador de la entidad asociado a una razón de descanso',
                example: 7
            },
            createBy: {
                type: 'number',
                description: 'por quien fue creado',
                example: 7
            },
            updateBy: {
                type: 'number',
                description: 'por quien fue actualizado',
                example: 8
            },
            status: {
                type: 'boolean',
                description: 'estado de la razón de descanso',
                example: true
            },
            timeReason: {
                type: 'number',
                description: 'duración de la razón de descanso',
                example: 20
            },
        }
    },
}