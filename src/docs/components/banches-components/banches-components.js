module.exports = {
    createBranch: {
        type: 'object',
        properties: {
            branchName: {
                type: 'string',
                description: 'nombre de una sucursal',
                example: "bella vista"
            },
            description: {
                type: 'string',
                description: 'detalles sobre la sucursal',
                example: "San Miguelito"
            },
            entityId: {
                type: 'number',
                description: 'identificador de la entidad a la cual la sucursal está asociada',
                example: 2
            },

            maxDiffWaitTime: {
                type: 'number',
                description: 'tiempo limite',
                example: 10
            },
            maxWaitingTime: {
                type: 'number',
                description: 'tiempo de espera',
                example: 10
            },
            criticalWaitingTime: {
                type: 'number',
                description: 'tiempo critico',
                example: 5
            },
            createBy: {
                type: 'number',
                description: 'identificador de usuario quien creó la sucursal',
                example: 10
            },
            status: {
                type: 'boolean',
                description: 'estado de la sucursal (falso/verdadero',
                example: true
            },
            updateBy: {
                type: 'number',
                description: 'identidicador de quien actualizó la información de una sucursal',
                example: 2
            },
            timezone: {
                type: 'string',
                description: 'definicion del tiempo',
                example: 'America/Panama'
            },


            Banner: {
                type: 'string',
                description: 'imagen de banner',
                example: "https://cdn.getxplor.net/Fluyapp/clientes/e0e9fc1a-ea23-4c92-ae0b-a5d70ac96bc6.jpg"
            },

            isCapturePhoto: {
                type: 'boolean',
                description: 'si debe capturar una foto o no',
                example: false
            },

            siteId: {
                type: 'number',
                description: 'identiicador del sitio para fluyapp app',
                example: '5f04cb2653621630ef9046e3'
            },

            latitud: {
                type: 'float',
                description: 'ubicacion según la latitud de la sucursal',
                example: 8.9971155
            },

            longitud: {
                type: 'float',
                description: 'ubicacion según la longitud de la sucursal',
                example: -79.5846823
            },
        },
    },
    responseBodyNewBranch: {
        type: 'object',
        properties: {
            branchName: {
                type: 'string',
                description: 'nombre de una sucursal',
                example: "bella vista"
            },
            description: {
                type: 'string',
                description: 'detalles sobre la sucursal',
                example: "San Miguelito"
            },
            entityId: {
                type: 'number',
                description: 'identificador de la entidad a la cual la sucursal está asociada',
                example: 2
            },

            maxDiffWaitTime: {
                type: 'number',
                description: 'tiempo limite',
                example: 10
            },
            maxWaitingTime: {
                type: 'number',
                description: 'tiempo de espera',
                example: 10
            },
            criticalWaitingTime: {
                type: 'number',
                description: 'tiempo critico',
                example: 5
            },
            createBy: {
                type: 'number',
                description: 'identificador de usuario quien creó la sucursal',
                example: 10
            },
            timezone: {
                type: 'string',
                description: 'definicion del tiempo',
                example: 'America/Panama'
            },
            isCapturePhoto: {
                type: 'boolean',
                description: 'si debe capturar una foto o no',
                example: false
            },
            latitud: {
                type: 'float',
                description: 'ubicacion según la latitud de la sucursal',
                example: 8.9971155
            },
            longitud: {
                type: 'float',
                description: 'ubicacion según la latitud de la sucursal',
                example: -79.5846823
            }
        },
    },
    responnseBranchCreated: {
        type: 'object',
        properties: {
            branchName: {
                type: 'string',
                description: 'nombre de una sucursal',
                example: "bella vista"
            },
            description: {
                type: 'string',
                description: 'detalles sobre la sucursal',
                example: "San Miguelito"
            },
            entityId: {
                type: 'number',
                description: 'identificador de la entidad a la cual la sucursal está asociada',
                example: 2
            },

            maxDiffWaitTime: {
                type: 'number',
                description: 'tiempo limite',
                example: 10
            },
            maxWaitingTime: {
                type: 'number',
                description: 'tiempo de espera',
                example: 10
            },
            criticalWaitingTime: {
                type: 'number',
                description: 'tiempo critico',
                example: 5
            },
            createBy: {
                type: 'number',
                description: 'identificador de usuario quien creó la sucursal',
                example: 10
            },
            status: {
                type: 'boolean',
                description: 'estado de la sucursal (falso/verdadero',
                example: true
            },
            updateBy: {
                type: 'number',
                description: 'identidicador de quien actualizó la información de una sucursal',
                example: 10
            },
            timezone: {
                type: 'string',
                description: 'definicion del tiempo',
                example: 'America/Panama'
            },


            Banner: {
                type: 'string',
                description: 'imagen de banner',
                example: ""
            },

            isCapturePhoto: {
                type: 'boolean',
                description: 'si debe capturar una foto o no',
                example: false
            },

            siteId: {
                type: 'number',
                description: 'identiicador del sitio para fluyapp app',
                example: ''
            },
            latitud: {
                type: 'float',
                description: 'ubicacion según la latitud de la sucursal',
                example: 8.9971155
            },
            longitud: {
                type: 'float',
                description: 'ubicacion según la longitud de la sucursal',
                example: -79.5846823
            },
        },
    },
    responseBodyBranchStatus: {
        type: 'object',
        properties: {
            status: {
                type: 'boolean',
                description: 'estado de la sucursal (falso/verdadero',
                example: true
            },
        }
    },
    responseBranchPagination: {
        type: 'object',
        properties: {
            branchName: {
                type: 'string',
                description: 'nombre de una sucursal',
                example: "bella vista"
            },
            description: {
                type: 'string',
                description: 'detalles sobre la sucursal',
                example: "San Miguelito"
            },
            entityId: {
                type: 'number',
                description: 'identificador de la entidad a la cual la sucursal está asociada',
                example: 2
            },

            maxDiffWaitTime: {
                type: 'number',
                description: 'tiempo limite',
                example: 10
            },
            maxWaitingTime: {
                type: 'number',
                description: 'tiempo de espera',
                example: 10
            },
            criticalWaitingTime: {
                type: 'number',
                description: 'tiempo critico',
                example: 5
            },
            createBy: {
                type: 'number',
                description: 'identificador de usuario quien creó la sucursal',
                example: 10
            },
            status: {
                type: 'boolean',
                description: 'estado de la sucursal (falso/verdadero',
                example: true
            },
            updateBy: {
                type: 'number',
                description: 'identidicador de quien actualizó la información de una sucursal',
                example: 2
            },
            timezone: {
                type: 'string',
                description: 'definicion del tiempo',
                example: 'America/Panama'
            },


            Banner: {
                type: 'string',
                description: 'imagen de banner',
                example: "https://cdn.getxplor.net/Fluyapp/clientes/e0e9fc1a-ea23-4c92-ae0b-a5d70ac96bc6.jpg"
            },

            isCapturePhoto: {
                type: 'boolean',
                description: 'si debe capturar una foto o no',
                example: false
            },

            siteId: {
                type: 'number',
                description: 'identiicador del sitio para fluyapp app',
                example: '5f04cb2653621630ef9046e3'
            },

            latitud: {
                type: 'float',
                description: 'ubicacion según la latitud de la sucursal',
                example: 8.9971155
            },

            longitud: {
                type: 'float',
                description: 'ubicacion según la longitud de la sucursal',
                example: -79.5846823
            },
            total: {
                type: 'number',
                description: 'cantidad total de las sucursales en la base de datos',
                example: 50
            }
        },
    },
    responseBranchView: {
        type: 'object',
        properties: {
            id: {
                type: 'number',
                description: 'identificador de la sucursal',
                example: 7
            },
            branchName: {
                type: 'string',
                description: 'nombre de la sucursal',
                example: 7
            },
            entityId: {
                type: 'number',
                description: 'identificador de la entidad asociado a una sucursal',
                example: 7
            },
            timezone: {
                type: 'string',
                description: 'zona horaria',
                example: 7
            },
        }
    }
}