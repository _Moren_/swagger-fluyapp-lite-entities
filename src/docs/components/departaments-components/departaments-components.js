module.exports = {
    createDepartament: {
        type: 'object',
        properties: {
            departamentName: {
                type: 'string',
                description: 'nombre del departamento',
                example: "Contabilidad"
            },
            description: {
                type: 'string',
                description: 'descripción del departamento',
                example: "Contabilidad"
            },
            entityId: {
                type: 'number',
                description: 'identificador de la entidad asociado a un departamento',
                example: 4
            },
            createBy: {
                type: 'number',
                description: 'ususario que crea el departamento',
                example: 7
            },
            updateBy: {
                type: 'number',
                description: 'ususario que actualiza el departamento',
                example: 7
            },
            branchId: {
                type: 'number',
                description: 'identificador de la sucursal',
                example: 9
            },
            state: {
                type: 'boolean',
                description: 'estasdo del departamento',
                example: 9
            },
        }
    },
    responseDepartamentList: {
        type: 'object',
        properties: {
            departamentName: {
                type: 'string',
                description: 'nombre del departamento',
                example: "Contabilidad"
            },
            branchId: {
                type: 'number',
                description: 'identificador de la sucursal',
                example: 9
            },
            id: {
                type: 'number',
                description: 'identificador del departamento',
                example: 9
            },
        }
    },
    responseDepartamentPagination: {
        type: 'object',
        properties: {
            departamentName: {
                type: 'string',
                description: 'nombre del departamento',
                example: "Contabilidad"
            },
            description: {
                type: 'string',
                description: 'descripción del departamento',
                example: "Contabilidad"
            },
            entityId: {
                type: 'number',
                description: 'identificador de la entidad asociado a un departamento',
                example: 4
            },
            createBy: {
                type: 'number',
                description: 'ususario que crea el departamento',
                example: 7
            },
            updateBy: {
                type: 'number',
                description: 'ususario que actualiza el departamento',
                example: 7
            },
            branchId: {
                type: 'number',
                description: 'identificador de la sucursal',
                example: 9
            },
            state: {
                type: 'boolean',
                description: 'estasdo del departamento',
                example: 9
            },
            total: {
                type: 'number',
                description: 'cantidad de registros de departamentos',
                example: 50
            }
        }
    },
    requesyBodyStateDepartament: {
        type: 'object',
        properties: {
            state: {
                type: 'boolean',
                description: 'estasdo del departamento',
                example: false
            },
        }
    },
    requesyBodyUpdateValueFromDepartament: {
        type: 'object',
        properties: {
            state: {
                type: 'boolean',
                description: 'estasdo del departamento',
                example: false
            },
            branchId: {
                type: 'number',
                description: 'identificador de la sucursal',
                example: 9
            },
            updateBy: {
                type: 'number',
                description: 'ususario que actualiza el departamento',
                example: 7
            },
            entityId: {
                type: 'number',
                description: 'identificador de la entidad asociado a un departamento',
                example: 4
            },
            departamentName: {
                type: 'string',
                description: 'nombre del departamento',
                example: "Contabilidad"
            }
        }
    },
}