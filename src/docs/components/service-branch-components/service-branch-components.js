module.exports = {
    createServiceToBranch: {
        type: 'object',
        properties: {
            serviceName: {
                type: 'string',
                description: 'nombre descriptivo para un servicio de una sucursal',
                example: 'ON THE WAY PANAMÁ'
            },
            description: {
                type: 'string',
                description: 'Detalles del servicio',
                example: 'ON THE WAY PANAMÁ'
            },
            queueServiceShort: {
                type: 'string',
                description: 'nomenclatura del servicio',
                example: 'V'
            },
            branchId: {
                type: 'number',
                description: 'identificador de la sucursal asociada a un servicio',
                example: 7
            },
            entityId: {
                type: 'number',
                description: 'identificador de la entidad asociada a una servicio',
                example: 6
            },
            aht: {
                type: 'number',
                description: 'duración del servicio',
                example: 10
            },
            serviceId: {
                type: 'number',
                description: 'identificador de un servicio',
                example: 36
            },
            awt: {
                type: 'number',
                description: 'duración del tiempo de espera',
                example: 5
            },
            ahtMax: {
                type: 'number',
                description: 'máximo tiempo de atención de un servicio',
                example: 10
            },
            awtMin: {
                type: 'number',
                description: 'timpo mínimo de espera de un servicio',
                example: 10
            },
            awtMax: {
                type: 'number',
                description: 'timpo máximo de espera de un servicio',
                example: 10
            },
            comment: {
                type: 'string',
                description: 'comentario sobre un servicio',
                example: 'Prueba de comentario'
            },
            isAppointment: {
                type: 'boolean',
                description: 'indica si el servicio es tipo cita o turno',
                example: true
            },
            createBy: {
                type: 'number',
                description: 'quien creó el servicio',
                example: 5
            },
            updateBy: {
                type: 'number',
                description: 'quien actualizó el servicio',
                example: 5
            },
            status: {
                type: 'boolean',
                description: 'habilita o deshabilita el servicio',
                example: false
            }
        }
    },
    responseServiceToBranch: {
        type: 'object',
        properties: {
            serviceName: {
                type: 'string',
                description: 'nombre descriptivo para un servicio de una sucursal',
                example: 'ON THE WAY PANAMÁ'
            },
            branchId: {
                type: 'number',
                description: 'identificador de la sucursal asociada a un servicio',
                example: 7
            },
            aht: {
                type: 'number',
                description: 'duración del servicio',
                example: 10
            },
            entityId: {
                type: 'number',
                description: 'identificador de la entidad asociada a una servicio',
                example: 6
            },
            serviceId: {
                type: 'number',
                description: 'identificador de un servicio',
                example: 36
            },
            aht: {
                type: 'number',
                description: 'duración del servicio',
                example: 10
            }
        }
    },
    responseListTimeServByAht: { // ***borrar en caso de que no se use****
        type: 'object',
        properties: {
            id: {
                type: 'number',
                description: 'identificador del servicio',
                example: 1
            },
            aht: {
                type: 'number',
                description: 'duración del servicio',
                example: 10
            },
            serviceId: {
                type: 'number',
                description: 'identificador de un servicio',
                example: 36
            },
            awt: {
                type: 'number',
                description: 'duración del tiempo de espera',
                example: 5
            },
            ahtMax: {
                type: 'number',
                description: 'máximo tiempo de atención de un servicio',
                example: 10
            },
            awtMin: {
                type: 'number',
                description: 'timpo mínimo de espera de un servicio',
                example: 10
            },
            awtMax: {
                type: 'number',
                description: 'timpo máximo de espera de un servicio',
                example: 10
            },
        }
    }
}