module.exports = {
    createService: {
        type: 'object',
        properties: {
            serviceName: {
                type: 'string',
                description: 'nombre descriptivo para un servicio',
                example: 'Ventas'
            },
            entityId: {
                type: 'number',
                description: 'identificador de la entidad asociado a un servicio',
                example: 7
            },
            description: {
                type: 'string',
                description: 'Detalles del servicio',
                example: 'Ventas'
            },
            price: {
                type: 'float',
                description: 'total costo del servicio',
                example: 10.00
            },
            queueServiceShort: {
                type: 'string',
                description: 'nomenclatura del servicio',
                example: 'V'
            },
            branchId: {
                type: 'number',
                description: 'identificador de la sucursal asociada a un servicio',
                example: 7
            },
            departamentId: {
                type: 'number',
                description: 'identificador de la departamento asociado a un servicio',
                example: 9
            },
            aht: {
                type: 'number',
                description: 'duración del servicio',
                example: 10
            },
            OfficeNumber: {
                type: 'number',
                description: 'número de la oficina',
                example: 201
            },
            awt: {
                type: 'number',
                description: 'duración del tiempo de espera',
                example: 5
            },
            ahtMax: {
                type: 'number',
                description: 'máximo tiempo de atención de un servicio',
                example: 10
            },
            ServiceTypeAppointmentMng: {
                type: 'string',
                description: 'habilita el tipo de servicio',
                example: [
                    'disable', 'enable'
                ]
            },

            ServiceTypeTurnMng: {
                type: 'string',
                description: 'habilita el tipo de servicio',
                example: [
                    'disable', 'enable'
                ]
            },
            requirements: {
                type: 'array',
                description: 'tipo de documentos que son requeridos',
                example: [
                    'cédula', 'pasaporte', 'certificado'
                ]
            },
            type: {
                type: 'string',
                description: '', //colocar despues su descripcion
                example: '' //colocar despues su ejemplo
            },
            queueId: {
                type: 'string',
                description: 'identificador de la cola al que está asociado un servicio',
                example: '5f05d45e7aa9fb3cc3414852'
            },


            awtMin: {
                type: 'number',
                description: 'timpo mínimo de espera de un servicio',
                example: 10
            },
            awtMax: {
                type: 'number',
                description: 'timpo máximo de espera de un servicio',
                example: 10
            },
            comment: {
                type: 'string',
                description: 'comentario sobre un servicio',
                example: 'Prueba de comentario'
            },
            isAppointment: {
                type: 'boolean',
                description: 'indica si el servicio es tipo cita o turno',
                example: true
            },
            createBy: {
                type: 'number',
                description: 'quien creo el servicio',
                example: 4
            },
            updateBy: {
                type: 'number',
                description: 'quien actualizó el servicio',
                example: 5
            },

            isShowKiosk: {
                type: 'boolean',
                description: 'si se debe mostrar o no el servicio de un kiosco',
                example: false
            },
            status: {
                type: 'boolean',
                description: 'habilita o deshabilita el servicio',
                example: false
            }

        }
    },
    requestBodyServiceUpdate: {
        type: 'object',
        properties: {
            serviceName: {
                type: 'string',
                description: 'nombre descriptivo para un servicio',
                example: 'Ventas'
            },
            description: {
                type: 'string',
                description: 'Detalles del servicio',
                example: 'Ventas'
            },
            price: {
                type: 'float',
                description: 'total costo del servicio',
                example: 10.00
            },
            queueServiceShort: {
                type: 'string',
                description: 'nomenclatura del servicio',
                example: 'V'
            },
            branchId: {
                type: 'number',
                description: 'identificador de la sucursal asociada a un servicio',
                example: 7
            },
            departamentId: {
                type: 'number',
                description: 'identificador de la departamento asociado a un servicio',
                example: 9
            },
            aht: {
                type: 'number',
                description: 'duración del servicio',
                example: 10
            },
            OfficeNumber: {
                type: 'number',
                description: 'número de la oficina',
                example: 201
            },
            awt: {
                type: 'number',
                description: 'duración del tiempo de espera',
                example: 5
            },
            ahtMax: {
                type: 'number',
                description: 'máximo tiempo de atención de un servicio',
                example: 10
            },
            ServiceTypeAppointmentMng: {
                type: 'string',
                description: 'habilita el tipo de servicio',
                example: [
                    'disable', 'enable'
                ]
            },

            ServiceTypeTurnMng: {
                type: 'string',
                description: 'habilita el tipo de servicio',
                example: [
                    'disable', 'enable'
                ]
            },
            requirements: {
                type: 'array',
                description: 'tipo de documentos que son requeridos',
                example: [
                    'cédula', 'pasaporte', 'certificado'
                ]
            },
            type: {
                type: 'string',
                description: '', //colocar despues su descripcion
                example: '' //colocar despues su ejemplo
            },
            queueId: {
                type: 'string',
                description: 'identificador de la cola al que está asociado un servicio',
                example: '5f05d45e7aa9fb3cc3414852'
            },


            awtMin: {
                type: 'number',
                description: 'timpo mínimo de espera de un servicio',
                example: 10
            },
            awtMax: {
                type: 'number',
                description: 'timpo máximo de espera de un servicio',
                example: 10
            },
            comment: {
                type: 'string',
                description: 'comentario sobre un servicio',
                example: 'Prueba de comentario'
            },
            isAppointment: {
                type: 'boolean',
                description: 'indica si el servicio es tipo cita o turno',
                example: true
            },
            updateBy: {
                type: 'number',
                description: 'quien actualizó el servicio',
                example: 5
            },

            isShowKiosk: {
                type: 'boolean',
                description: 'si se debe mostrar o no el servicio de un kiosco',
                example: false
            },
            status: {
                type: 'boolean',
                description: 'habilita o deshabilita el servicio',
                example: false
            }

        }
    },
    requestBodyStatusService: {
        type: 'object',
        properties: {
            status: {
                type: 'boolean',
                description: 'habilita o deshabilita el servicio',
                example: false
            }
        }
    },
    responseRequestService: {
        type: 'object',
        properties: {
            id: {
                type: 'number',
                description: 'identificador del servicio',
                example: 1
            },
            departamentId: {
                type: 'number',
                description: 'identificador de la departamento asociado a un servicio',
                example: 9
            },
            serviceName: {
                type: 'string',
                description: 'nombre descriptivo para un servicio',
                example: 'Ventas'
            },
            branchId: {
                type: 'number',
                description: 'identificador de la sucursal asociada a un servicio',
                example: 7
            },
            price: {
                type: 'float',
                description: 'total costo del servicio',
                example: 10.00
            }
        }
    },
    responseListingService: {
        type: 'object',
        properties: {
            id: {
                type: 'number',
                description: 'identificador del servicio',
                example: 1
            },
            serviceName: {
                type: 'string',
                description: 'nombre descriptivo para un servicio',
                example: 'Ventas'
            },
            branchId: {
                type: 'number',
                description: 'identificador de la sucursal asociada a un servicio',
                example: 7
            },
            departamentId: {
                type: 'number',
                description: 'identificador de la departamento asociado a un servicio',
                example: 9
            },
        }
    },
    responseGettinggService: {
        type: 'object',
        properties: {
            id: {
                type: 'number',
                description: 'identificador del servicio',
                example: 1
            },
            price: {
                type: 'float',
                description: 'precio de un servicio',
                example: 'Ventas'
            },
            serviceName: {
                type: 'string',
                description: 'nombre descriptivo para un servicio',
                example: 'Ventas'
            }
        }
    }
}

