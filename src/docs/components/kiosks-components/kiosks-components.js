module.exports = {
    createKiosk: {
        type: 'object',
        properties: {
            TimeAccess: {
                type: 'number',
                description: 'tiempo de acceso que ha transcurrido',
                example: 600
            },
            ApplyTimeAccess: {
                type: 'boolean',
                description: 'indicador de que si el kiosco aplicará el tiempo de acceso o no',
                example: false
            },
            nameKiosk: {
                type: 'string',
                description: 'nombre descriptivo del kiosco',
                example: 'KIOSCO #4'
            },
            Description: {
                type: 'string',
                description: 'detalles sobre el kiosco',
                example: 'KIOSCO #4'
            },
            entityId: {
                type: 'number',
                description: 'identificador de la entidad a la que pertenece el agente',
                example: 7
            },

        }
    },
    requestBodyKioskToUpdate: {
        type: 'object',
        properties: {
            TimeAccess: {
                type: 'number',
                description: 'tiempo de acceso que ha transcurrido',
                example: 600
            },
            ApplyTimeAccess: {
                type: 'boolean',
                description: 'indicador de que si el kiosco aplicará el tiempo de acceso o no',
                example: false
            },
            nameKiosk: {
                type: 'string',
                description: 'nombre descriptivo del kiosco',
                example: 'KIOSCO #4'
            },
            Description: {
                type: 'string',
                description: 'detalles sobre el kiosco',
                example: 'KIOSCO #4'
            },
        }
    },
    gettingAttributesKiosk: {
        type: 'object',
        properties: {
            TimeAccess: {
                type: 'number',
                description: 'tiempo de acceso que ha transcurrido',
                example: 600
            },
            ApplyTimeAccess: {
                type: 'boolean',
                description: 'indicador de que si el kiosco aplicará el tiempo de acceso o no',
                example: false
            },
        }
    }
}