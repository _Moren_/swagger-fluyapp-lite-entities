module.exports = {
    creatreModuleAgent: {
        type: 'object',
        properties: {
            agentId: {
                type: 'number',
                description: 'identificador del agente',
                example: 6
            },
            entityId: {
                type: 'number',
                description: 'identificador de la entidad',
                example: 4
            },
            branchId: {
                type: 'number',
                description: 'identificador de la sucursal',
                example: 5
            },
            moduleId: {
                type: 'number',
                description: 'identificador del módulo',
                example: 8
            },
            nameModule: {
                type: 'string',
                description: 'nombre del módulo',
                example: 'Modulo 105'
            },
            numberModule: {
                type: 'number',
                description: 'numero del modulo',
                example: 105
            },
        }
    },
    requestBodyModuleAgent: {
        type: 'object',
        properties: {
            agentId: {
                type: 'number',
                description: 'identificador del agente',
                example: 6
            },
            entityId: {
                type: 'number',
                description: 'identificador de la entidad',
                example: 4
            },
            branchId: {
                type: 'number',
                description: 'identificador de la sucursal',
                example: 5
            },
        }
    }
}