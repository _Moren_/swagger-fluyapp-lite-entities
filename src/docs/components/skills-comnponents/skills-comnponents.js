module.exports = {
    createSkills: {
        type: 'object',
        properties: {
            userId: {
                type: 'number',
                description: 'identificador de un usuario',
                example: 40
            },
            branchId: {
                type: 'number',
                description: 'identificador de la sucursal asociada a un servicio',
                example: 7
            },
            entityId: {
                type: 'number',
                description: 'identificador de una entidad',
                example: 6
            },
            departamentId: {
                type: 'number',
                description: 'identificador de un departamento asociada a una servicio',
                example: 6
            },
            serviceId: {
                type: 'number',
                description: 'identificador de un servicio',
                example: 36
            },
            createBy: {
                type: 'number',
                description: 'quién creó la habilidad',
                example: 1
            },
            updateBy: {
                type: 'number',
                description: 'quién actualió la habilidad',
                example: 1
            },
            status: {
                type: 'boolean',
                description: 'estado de la habilidad',
                example: true
            },
        }
    },
    responseSkill: {
        type: 'object',
        properties: {
            id: {
                type: 'number',
                description: 'identificador del servicio',
                example: 2
            },
            userId: {
                type: 'number',
                description: 'identificador de un usuario',
                example: 40
            },
            branchId: {
                type: 'number',
                description: 'identificador de la sucursal asociada a un servicio',
                example: 7
            },
            entityId: {
                type: 'number',
                description: 'identificador de una entidad',
                example: 6
            },
            departamentId: {
                type: 'number',
                description: 'identificador de un departamento asociada a una servicio',
                example: 6
            },
            serviceId: {
                type: 'number',
                description: 'identificador de un servicio',
                example: 36
            },
            status: {
                type: 'boolean',
                description: 'estado de la habilidad',
                example: true
            },
        }
    },
    requestBodySkillUpdated:{
        type: 'object',
        properties: {
            userId: {
                type: 'number',
                description: 'identificador de un usuario',
                example: 40
            },
            branchId: {
                type: 'number',
                description: 'identificador de la sucursal asociada a un servicio',
                example: 7
            },
            entityId: {
                type: 'number',
                description: 'identificador de una entidad',
                example: 6
            },
            departamentId: {
                type: 'number',
                description: 'identificador de un departamento asociada a una servicio',
                example: 6
            },
            serviceId: {
                type: 'number',
                description: 'identificador de un servicio',
                example: 36
            },
            updateBy: {
                type: 'number',
                description: 'quién actualió la habilidad',
                example: 1
            },
        }
    }
}