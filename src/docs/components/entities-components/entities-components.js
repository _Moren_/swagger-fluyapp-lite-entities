module.exports = {
    createEntity: {
        type: 'object',
        properties: {
            entityName: {
                type: 'string',
                description: 'nombre de la entidad',
                example: 'TORRES DE LAS AMERICAS'
            },
            description: {
                type: 'string',
                description: 'descripción breve de la entidad',
                example: 'TORRES DE LAS AMERICAS'
            },
            state: {
                type: 'boolean',
                description: 'maneja el estado de la entidad (verdadero/falso',
                example: true
            },
            idCountry: {
                type: 'number',
                description: 'identificador del pais',
                example: 2
            },
            createBy: {
                type: 'number',
                description: 'usuario quien creo la entidad',
                example: 1
            },

            updateBy: {
                type: 'number',
                description: 'usuario quien actualizó la entidad',
                example: 2
            },
            check_directory: {
                type: 'boolean',
                description: 'para saber si manejará archivos tipo imagen',
                example: true
            },
            check_pregister: {
                type: 'boolean',
                description: 'para saber si manejara identificacion',
                example: false
            },
            typeUse: {
                type: 'number',
                description: 'saber que tipo de usuario es',
                example: 1
            },
            category: {
                type: 'string',
                description: 'para saber a qué categoria pertenece en fluyapp app',
                example: "5a3424b9b574d8461a052733"
            },
            _clientID: {
                type: 'string',
                description: 'identificador de cliente para ser usado en fluyapp app',
                example: "5efdf6e2e9ce1562e6f1a1d7"
            },
            BannerBig: {
                type: 'string',
                description: 'para las imagenes dentro de fluyapp app',
                example: "https://cdn.getxplor.net/Fluyapp/clientes/e0e9fc1a-ea23-4c92-ae0b-a5d70ac96bc6.jpg"
            },
            logoBanner: {
                description: 'para las imagenes dentro de fluyapp app',
                example: "https://cdn.getxplor.net/Fluyapp/clientes/7ef3e377-fed1-4a3b-9fa5-ce69b615c70f.jpg"
            },
            bgBannerURL: {
                description: 'para las imagenes dentro de fluyapp app',
                example: "https://cdn.getxplor.net/Fluyapp/clientes/726708b9-294d-4a26-985a-d8e8490e4d9a.jpg"
            },
            logoURL: {
                type: 'string',
                description: 'para las imagenes dentro de fluyapp app',
                example: "https://cdn.getxplor.net/Fluyapp/clientes/787c06d2-ce29-4981-97e6-f15975ff361a.jpg"
            },
            Network_Public: {
                type: 'string',
                description: 'para saber el tipo de red',
                example: "http://100.11.60.100:9000"
            },
            Network_private: {
                type: 'string',
                description: 'para saber el tipo de red',
                example: "http://100.11.60.100:9000"
            },
            IsTypeNetwork: {
                type: 'number',
                description: 'para saber el tipo de red',
                example: 2
            },
        }
    },
    responsesPagination: {
        type: 'object',
        properties: {
            entityName: {
                type: 'string',
                description: 'nombre de la entidad',
                example: 'TORRES DE LAS AMERICAS'
            },
            description: {
                type: 'string',
                description: 'descripción breve de la entidad',
                example: 'TORRES DE LAS AMERICAS'
            },
            state: {
                type: 'boolean',
                description: 'maneja el estado de la entidad (verdadero/falso',
                example: true
            },
            idCountry: {
                type: 'number',
                description: 'identificador del pais',
                example: 2
            },
            createBy: {
                type: 'number',
                description: 'usuario quien creo la entidad',
                example: 1
            },

            updateBy: {
                type: 'number',
                description: 'usuario quien actualizó la entidad',
                example: 2
            },
            check_directory: {
                type: 'boolean',
                description: 'para saber si manejará archivos tipo imagen',
                example: true
            },
            check_pregister: {
                type: 'boolean',
                description: 'para saber si manejara identificacion',
                example: false
            },
            typeUse: {
                type: 'number',
                description: 'saber que tipo de usuario es',
                example: 1
            },
            category: {
                type: 'string',
                description: 'para saber a qué categoria pertenece en fluyapp app',
                example: "5a3424b9b574d8461a052733"
            },
            _clientID: {
                type: 'string',
                description: 'identificador de cliente para ser usado en fluyapp app',
                example: "5efdf6e2e9ce1562e6f1a1d7"
            },
            BannerBig: {
                type: 'string',
                description: 'para las imagenes dentro de fluyapp app',
                example: "https://cdn.getxplor.net/Fluyapp/clientes/e0e9fc1a-ea23-4c92-ae0b-a5d70ac96bc6.jpg"
            },
            logoBanner: {
                description: 'para las imagenes dentro de fluyapp app',
                example: "https://cdn.getxplor.net/Fluyapp/clientes/7ef3e377-fed1-4a3b-9fa5-ce69b615c70f.jpg"
            },
            bgBannerURL: {
                description: 'para las imagenes dentro de fluyapp app',
                example: "https://cdn.getxplor.net/Fluyapp/clientes/726708b9-294d-4a26-985a-d8e8490e4d9a.jpg"
            },
            logoURL: {
                type: 'string',
                description: 'para las imagenes dentro de fluyapp app',
                example: "https://cdn.getxplor.net/Fluyapp/clientes/787c06d2-ce29-4981-97e6-f15975ff361a.jpg"
            },
            Network_Public: {
                type: 'string',
                description: 'para saber el tipo de red',
                example: "http://100.11.60.100:9000"
            },
            Network_private: {
                type: 'string',
                description: 'para saber el tipo de red',
                example: "http://100.11.60.100:9000"
            },
            IsTypeNetwork: {
                type: 'number',
                description: 'para saber el tipo de red',
                example: 2
            },
            total: {
                type: 'number',
                description: 'total de registros de entidades en la base de datos',
                example: 40
            }
        }
    },
    responseBodyEntity: {
        type: 'object',
        properties: {
            entityName: {
                type: 'string',
                description: 'nombre de la entidad',
                example: 'TORRES DE LAS AMERICAS'
            },
            description: {
                type: 'string',
                description: 'descripción breve de la entidad',
                example: 'TORRES DE LAS AMERICAS'
            },
            idCountry: {
                type: 'number',
                description: 'identificador del pais',
                example: 2
            },
            direction: {
                type: 'number',
                description: 'ubicacion fisica de la entidad',
                example: "Panama Capital"
            },
            createBy: {
                type: 'number',
                description: 'usuario quien creo la entidad',
                example: 1
            },

            updateBy: {
                type: 'number',
                description: 'usuario quien actualizó la entidad',
                example: 2
            },
            check_directory: {
                type: 'boolean',
                description: 'para saber si manejará archivos tipo imagen',
                example: true
            },
            check_pregister: {
                type: 'boolean',
                description: 'para saber si manejara identificacion',
                example: false
            },
            typeUse: {
                type: 'number',
                description: 'saber que tipo de usuario es',
                example: 1
            },
            Network_Public: {
                type: 'string',
                description: 'para saber el tipo de red',
                example: "http://100.11.60.100:9000"
            },
            Network_private: {
                type: 'string',
                description: 'para saber el tipo de red',
                example: "http://100.11.60.100:9000"
            },
            IsTypeNetwork: {
                type: 'number',
                description: 'para saber el tipo de red',
                example: 2
            },
        }
    },
    entityesCreated: {
        type: 'object',
        properties: {
            entityName: {
                type: 'string',
                description: 'nombre de la entidad',
                example: 'TORRES DE LAS AMERICAS'
            },
            description: {
                type: 'string',
                description: 'descripción breve de la entidad',
                example: 'TORRES DE LAS AMERICAS'
            },
            state: {
                type: 'boolean',
                description: 'maneja el estado de la entidad (verdadero/falso',
                example: true
            },
            idCountry: {
                type: 'number',
                description: 'identificador del pais',
                example: 2
            },
            createBy: {
                type: 'number',
                description: 'usuario quien creo la entidad',
                example: 1
            },

            updateBy: {
                type: 'number',
                description: 'usuario quien actualizó la entidad',
                example: 2
            },
            check_directory: {
                type: 'boolean',
                description: 'para saber si manejará archivos tipo imagen',
                example: true
            },
            check_pregister: {
                type: 'boolean',
                description: 'para saber si manejara identificacion',
                example: true
            },
            typeUse: {
                type: 'number',
                description: 'saber que tipo de usuario es',
                example: ''
            },
            category: {
                type: 'string',
                description: 'para saber a qué categoria pertenece en fluyapp app',
                example: ""
            },
            _clientID: {
                type: 'string',
                description: 'identificador de cliente para ser usado en fluyapp app',
                example: ""
            },
            BannerBig: {
                type: 'string',
                description: 'para las imagenes dentro de fluyapp app',
                example: ""
            },
            logoBanner: {
                description: 'para las imagenes dentro de fluyapp app',
                example: ""
            },
            bgBannerURL: {
                description: 'para las imagenes dentro de fluyapp app',
                example: ""
            },
            logoURL: {
                type: 'string',
                description: 'para las imagenes dentro de fluyapp app',
                example: ""
            },
            Network_Public: {
                type: 'string',
                description: 'para saber el tipo de red',
                example: "http://100.11.60.100:9000"
            },
            Network_private: {
                type: 'string',
                description: 'para saber el tipo de red',
                example: "http://100.11.60.100:9000"
            },
            IsTypeNetwork: {
                type: 'number',
                description: 'para saber el tipo de red',
                example: 2
            },
        }
    },
    responsesCheckingEntity: {
        type: 'object',
        properties: {
            check_directory: {
                type: 'boolean',
                description: 'para saber si manejará archivos tipo imagen',
                example: true
            },
            check_pregister: {
                type: 'boolean',
                description: 'para saber si manejara identificacion',
                example: false
            },
        }
    },
    gettingNameEntity: {
        type: 'object',
        properties: {
            entityName: {
                type: 'string',
                description: 'nombre de la entidad',
                example: true
            },
            _clientID: {
                type: 'string',
                description: 'identificador de cliente para fluyapp app',
                example: "5efdf6e2e9ce1562e6f1a1d7"
            },
        }
    },
    responsesEntityState: {
        type: 'object',
        properties: {
            state: {
                type: 'boolean',
                description: 'valor del estado',
                example: true
            }
        }
    },
    updateEntity: {
        type: 'object',
        properties: {
            entityName: {
                type: 'string',
                description: 'nombre de la entidad',
                example: 'TORRES DE LAS AMERICAS'
            },
            description: {
                type: 'string',
                description: 'descripción breve de la entidad',
                example: 'TORRES DE LAS AMERICAS'
            },
            idCountry: {
                type: 'number',
                description: 'identificador del pais',
                example: 2
            },
            updateBy: {
                type: 'number',
                description: 'usuario quien actualizó la entidad',
                example: 2
            },
            check_directory: {
                type: 'boolean',
                description: 'para saber si manejará archivos tipo imagen',
                example: true
            },
            check_pregister: {
                type: 'boolean',
                description: 'para saber si manejara identificacion',
                example: false
            },
            category: {
                type: 'string',
                description: 'para saber a qué categoria pertenece en fluyapp app',
                example: "5a3424b9b574d8461a052733"
            },
            _clientID: {
                type: 'string',
                description: 'identificador de cliente para ser usado en fluyapp app',
                example: "5efdf6e2e9ce1562e6f1a1d7"
            },
            BannerBig: {
                type: 'string',
                description: 'para las imagenes dentro de fluyapp app',
                example: "https://cdn.getxplor.net/Fluyapp/clientes/e0e9fc1a-ea23-4c92-ae0b-a5d70ac96bc6.jpg"
            },
            logoBanner: {
                description: 'para las imagenes dentro de fluyapp app',
                example: "https://cdn.getxplor.net/Fluyapp/clientes/7ef3e377-fed1-4a3b-9fa5-ce69b615c70f.jpg"
            },
            bgBannerURL: {
                description: 'para las imagenes dentro de fluyapp app',
                example: "https://cdn.getxplor.net/Fluyapp/clientes/726708b9-294d-4a26-985a-d8e8490e4d9a.jpg"
            },
            logoURL: {
                type: 'string',
                description: 'para las imagenes dentro de fluyapp app',
                example: "https://cdn.getxplor.net/Fluyapp/clientes/787c06d2-ce29-4981-97e6-f15975ff361a.jpg"
            },
            Network_Public: {
                type: 'string',
                description: 'para saber el tipo de red',
                example: "http://100.11.60.100:9000"
            },
            Network_private: {
                type: 'string',
                description: 'para saber el tipo de red',
                example: "http://100.11.60.100:9000"
            },
            IsTypeNetwork: {
                type: 'number',
                description: 'para saber el tipo de red',
                example: 2
            },
        }
    },
    litstingEntityValueToKiosk: {
        type: 'object',
        properties: {
            id: {
                type: 'number',
                description: 'identificador de la entidad',
                example: 4
            },
            entityName: {
                type: 'string',
                description: 'nombre de la entidad',
                example: 'TORRES DE LAS AMERICAS'
            },
            IsTypeNetwork: {
                type: 'number',
                description: 'para saber el tipo de red',
                example: 2
            },
        }
    },
    gettingNetWorkingField: {
        type: 'object',
        properties: {
            Network_Public: {
                type: 'string',
                description: 'para saber el tipo de red',
                example: "http://100.11.60.100:9000"
            },
            Network_private: {
                type: 'string',
                description: 'para saber el tipo de red',
                example: "http://121.20.60.107:8700"
            },
            IsTypeNetwork: {
                type: 'number',
                description: 'para saber el tipo de red',
                example: 2
            },
        }
    },
    responseBodyNetWorkEntityId: {
        type: 'object',
        properties: {
            entityId: {
                type: 'number',
                description: 'identificador de la entidad',
                example: 4
            }
        }
    }
}