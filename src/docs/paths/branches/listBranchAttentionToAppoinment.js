module.exports = {
    get: {
        tags: ['branches'],
        description: "lista todas las sucursales segúne el identificador de la sucursal y la entidad",
        operationId: "listBranchAttentionToAppoinment",
        parameters: [
            {
                in: 'query',
                name: 'branchId'
            },
            {
                in: 'query',
                name: 'entityId'
            },
        ],
        responses: {
            'API_B_200': {
                description: 'Detalle de la sucursal.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/responseBranchView'
                        }
                    }
                }
            },
            'API_B_404': {
                description: 'No existe esa sucursal.',
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
            'API_E_005': {
                description: 'Ya existe una entidad con este nombre.'
            },
        }
    }
}