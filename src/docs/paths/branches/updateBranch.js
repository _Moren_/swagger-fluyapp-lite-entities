module.exports = {
    put: {
        tags: ['branches'],
        description: "actualiza los datos de una sucursal",
        operationId: "updateBranch",
        parameters: [
            {
                in: 'params',
                name: 'id'
            }
        ],
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/responseBodyNewBranch'
                    }
                }
            }
        },
        responses: {
            'API_B_200': {
                description: 'Sucursal actualizada con éxito',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/responnseBranchCreated'
                        }
                    }
                }
            },
            'API_B_404': {
                description: 'El código de la Sucursal no existe.'
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
            'API_E_005': {
                description: 'Ya existe una entidad con este nombre.'
            },
        }
    }
}