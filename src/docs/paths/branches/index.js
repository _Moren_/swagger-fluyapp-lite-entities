const createBranch = require('./createBranch');
const updateBranch = require('./updateBranch');
const changeStatusBranch = require('./changeStatusBranch');
const getBranchId = require('./getBranchId');
const branchesByEntity = require('./branchesByEntity');
const listsBranchesToKiosko = require('./listsBranchesToKiosko');
const listBranchesOnView = require('./listBranchesOnView');
const listBranchAttentionToAppoinment = require('./listBranchAttentionToAppoinment');

module.exports = {
    '/branch-create/': {
        ...createBranch
    },
    '/branch-update/:id': {
        ...updateBranch
    },
    '/branch-change-status/:id': {
        ...changeStatusBranch
    },
    '/branch-view/:id': {
        ...getBranchId
    },
    '/branch-lists': {
        ...branchesByEntity
    },
    '/tower-kiosk': {
        ...listsBranchesToKiosko
    },
    '/branch-lists-ddl/': {
        ...listBranchesOnView
    },
    '/branch-lists-appoinment': {
        ...listBranchAttentionToAppoinment
    },

}