module.exports = {
    get: {
        tags: ['branches'],
        description: "muestra toda la información de las sucursales",
        operationId: "getBranchId",
        parameters: [
            {
                in: 'params',
                name: 'id'
            }
        ],
        responses: {
            'API_B_200': {
                description: 'Información de la Sucursal',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/responnseBranchCreated'
                        }
                    }
                }
            },
            'API_B_404': {
                description: 'No existe este registro de Sucursales',
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
            'API_E_005': {
                description: 'Ya existe una entidad con este nombre.'
            },
        }
    }
}