const createServiceToBranches = require('./createServiceToBranches');
const getServBranchByAgent = require('./getServBranchByAgent');
const updateServiceToBranch = require('./updateServiceToBranch')
const deleteServiceToBranch = require('./deleteServiceToBranch');
const listsServicesToBranchesByEntity = require('./listsServicesToBranchesByEntity');

module.exports = {
    '/serv_branch-create/': {
        ...createServiceToBranches
    },
    '/list-servbra-by-agent/': {
        ...getServBranchByAgent
    },
    '/list-servbra-by-entity/': {
        ...listsServicesToBranchesByEntity
    },
    '/serv_branch-update/:id': {
        ...updateServiceToBranch
    },
    '/serv_branch-delete/:id': {
        ...deleteServiceToBranch
    }
}