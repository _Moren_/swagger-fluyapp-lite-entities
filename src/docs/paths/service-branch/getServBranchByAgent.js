module.exports = {
    get: {
        tags: ['service-branches'],
        description: "Retorna ciertos valores de un servicio",
        operationId: "getServBranchByAgent",
        parameters: [
            {
                in:'query',
                name:'entityId'
            },
            {
                in:'query',
                name:'departamentId'
            },
            {
                in:'query',
                name:'branchId'
            },
        ],
        responses: {
            'API_SB_200': {
                description: 'información de Servicios.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/responseServiceToBranch'
                        }
                    }
                }
            },
            'API_SB_404': {
                description: 'No hay datos relacionados con estos valores.'
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
            'API_E_005': {
                description: 'Ya existe una entidad con este nombre.'
            },
        }
    }
}