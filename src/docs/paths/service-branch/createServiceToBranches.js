module.exports = {
    post: {
        tags: ['service-branches'],
        description: "Crea un servicio",
        operationId: "createServiceToBranches",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/createServiceToBranch'
                    }
                }
            }
        },
        responses: {
            'API_SB_200': {
                description: 'información de Servicios.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createServiceToBranch'
                        }
                    }
                }
            },
            'API_SB_200': {
                description: 'Registro del Servicio son éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createServiceToBranch'
                        }
                    }
                }
            },
            'API_SB_404': {
                description: 'Fallo al registrar un Servicio.'
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
            'API_E_005': {
                description: 'Ya existe una entidad con este nombre.'
            },
        }
    }
}