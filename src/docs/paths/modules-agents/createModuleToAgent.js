module.exports = {
    post: {
        tags: ['modules_agents'],
        description: "Crea y asigna los modulos para los agentes",
        operationId: "createModuleToAgent",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/creatreModuleAgent'
                    }
                }
            }
        },
        responses: {
            'API_MA_200': {
                description: 'Módulo asignado con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/creatreModuleAgent'
                        }
                    }
                }
            },
            'API_MA_403': {
                description: 'Este módulo ya está asignado a otro agente.'
            },
            'API_MA_404': {
                description: 'Algo sucedió al asignar un módulo.'
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
            'API_E_005': {
                description: 'Ya existe una entidad con este nombre.'
            },
        }
    }
}