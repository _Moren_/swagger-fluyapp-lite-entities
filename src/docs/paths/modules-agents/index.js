const createModuleToAgent = require('./createModuleToAgent');
const verifyAsignAgentModule = require('./verifyAsignAgentModule');

module.exports = {
    '/asign-module-agent/': {
        ...createModuleToAgent
    },
    '/verify-module/': {
        ...verifyAsignAgentModule
    }
}