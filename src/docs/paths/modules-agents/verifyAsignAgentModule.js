module.exports = {
    post: {
        tags: ['modules_agents'],
        description: "Verifica si el agente tiene un modulo asignado",
        operationId: "verifyAsignAgentModule",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/requestBodyModuleAgent'
                    }
                }
            }
        },
        responses: {
            'API_MA_200': {
                description: 'Módulo asignado.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/creatreModuleAgent'
                        }
                    }
                }
            },
            'API_MA_404': {
                description: 'No tiene módulo asignado.'
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
            'API_E_005': {
                description: 'Ya existe una entidad con este nombre.'
            },
        }
    }
}