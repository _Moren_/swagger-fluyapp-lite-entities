const registAgentReasonRest = require('./registAgentReasonRest');
const lastStateAgentReasonRest = require('./lastStateAgentReasonRest');

module.exports = {
    '/agent-asign-reason/': {
        ...registAgentReasonRest
    },
    '/agent-verify-reason/': {
        ...lastStateAgentReasonRest
    }
}