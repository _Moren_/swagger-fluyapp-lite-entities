module.exports = {
    post: {
        tags: ['agent_reasons'],
        description: "Registra información acerca de las razones de descando de un agente",
        operationId: "registAgentReasonRest",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/createAgentReason'
                    }
                }
            }
        },
        responses: {
            'API_MA_200': {
                description: 'Ha aplicado una razón de descanso.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/responseAgentReasonCreated'
                        }
                    }
                }
            },
            'API_MA_403': {
                description: 'Algo sucedió al regstrar su razón de descanso.'
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
            'API_E_005': {
                description: 'Ya existe una entidad con este nombre.'
            },
        }
    }
}