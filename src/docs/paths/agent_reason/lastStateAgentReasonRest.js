module.exports = {
    post: {
        tags: ['agent_reasons'],
        description: "Valida si el agente ha tenido información previa sobre algún tipo de descanso",
        operationId: "lastStateAgentReasonRest",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/requestBodyAgentReasonToFind'
                    }
                }
            }
        },
        responses: {
            'API_MA_200': {
                description: 'ultimo estado.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createAgentReason'
                        }
                    }
                }
            },
            'API_MA_403': {
                description: 'No tiene último estado registrado.'
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
            'API_E_005': {
                description: 'Ya existe una entidad con este nombre.'
            },
        }
    }
}