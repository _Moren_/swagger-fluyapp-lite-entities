module.exports = {
    get: {
        tags: ['kiosks'],
        description: "Retorna ciertos atributos sobre un kiosco",
        operationId: "getDetailKiosk",
        parameters: [
            {
                in: 'query',
                name: 'kioskoId'
            }
        ],
        responses: {
            'API_KS_200': {
                description: 'Detalle del kiosko.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/gettingAttributesKiosk'
                        }
                    }
                }
            },
            'API_KS_404': {
                description: 'No hay kioscos registrados bajo ese id.'
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
            'API_E_005': {
                description: 'Ya existe una entidad con este nombre.'
            },
        }
    }
}