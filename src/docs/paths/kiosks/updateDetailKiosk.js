module.exports = {
    put: {
        tags: ['kiosks'],
        description: "Actualiza la información acerca sobre un kiosco",
        operationId: "updateDetailKiosk",
        parameters: [
            {
                in: 'params',
                name: 'id'
            }
        ],
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/requestBodyKioskToUpdate'
                    }
                }
            }
        },
        responses: {
            'API_KS_200': {
                description: 'Kiosco actualizado con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createKiosk'
                        }
                    }
                }
            },
            'API_KS_403': {
                description: 'Kiosco actualizado con éxito.'
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
            'API_E_005': {
                description: 'Ya existe una entidad con este nombre.'
            },
        }
    }
}