const createKioskPregister = require('./createKioskPregister');
const updateDetailKiosk = require('./updateDetailKiosk');
const getDetailKiosk = require('./getDetailKiosk');
const listsDataKiosk = require('./listsDataKiosk');

module.exports = {
    '/kiosk-create/': {
        ...createKioskPregister
    },
    '/kiosk-update/:id': {
        ...updateDetailKiosk
    },
    '/kiosk-detail/': {
        ...getDetailKiosk
    },
    '/kiosk-lists': {
        ...listsDataKiosk
    }
}