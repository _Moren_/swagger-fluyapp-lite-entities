module.exports = {
    get: {
        tags: ['kiosks'],
        description: "Retorna todos los detalles de los kioscos en base al identificador de la entidad a la que pertenecen",
        operationId: "listsDataKiosk",
        parameters: [
            {
                in: 'query',
                name: 'entityId'
            }
        ],
        responses: {
            'API_KS_200': {
                description: 'Listado de los kiosko.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createKiosk'
                        }
                    }
                }
            },
            'API_KS_404': {
                description: 'No hay kioscos registrados bajo la entidad seleccionada.'
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
            'API_E_005': {
                description: 'Ya existe una entidad con este nombre.'
            },
        }
    }
}