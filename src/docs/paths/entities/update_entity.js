module.exports = {
    put: {
        tags: ['entities'],
        description: "Actualiza la información de una entidad según su id)",
        operationId: "update_entity",
        parameters: [
            {
                in: 'params',
                name: 'id'
            }
        ],
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/updateEntity'
                    }
                }
            }
        },
        responses: {
            'API_E_200': {
                description: 'Entidad actualizada con exito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/entityesCreated'
                        }
                    }
                }
            },
            'API_E_404': {
                description: 'El codigo de la Entidad no existe.'
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
            'API_E_005': {
                description: 'Ya existe una entidad con este nombre.'
            },


        }
    }
}