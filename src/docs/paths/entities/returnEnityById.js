module.exports = {
    get: {
        tags: ['entities'],
        description: "Retorna toda la informacion de una entidad en base a su identificador",
        operationId: "returnEnityById",
        parameters: [
            {
                in: 'params',
                name: 'id'
            }
        ],
        responses: {
            'API_E_200': {
                description: 'Detalle de la Entidad.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createEntity'
                        }
                    }
                }
            },
            'API_E_404': {
                description: 'No hay registros de esa Entidad.'
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
            'API_E_005': {
                description: 'Ya existe una entidad con este nombre.'
            },


        }
    }
}