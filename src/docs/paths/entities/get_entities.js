module.exports = {
    get: {
        tags: ['entities'],
        description: "Listado todas las entidades con paginación",
        operationId: "get_entities",
        parameters: [
            {
                in: 'query',
                name: 'limit'
            },
            {
                in: 'query',
                name: 'page'
            },
        ],
        responses: {
            'API_E_200': {
                description: 'Listado de Entidades con éxito',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/responsesPagination'
                        }
                    }
                }
            },
            'API_E_404': {
                description: 'No hay registros de alguna Entidad',
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },

            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
            'API_E_005': {
                description: 'Ya existe una entidad con este nombre.'
            },


        }
    }
}