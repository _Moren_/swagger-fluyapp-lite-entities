const entities = require('./createEntity');
const getCheckingEntity = require('./getCheckingEntity');
const checkPregister = require('./checkPregister');
const returnEnityById = require('./returnEnityById');
const get_entities = require('./get_entities');
const get_entity = require('./get_entity');
const changeStatusEntity = require('./changeStatusEntity');
const update_entity = require('./update_entity');
const listEntityOnSelect = require('./listEntityOnSelect');
const getNetWorkField = require('./getNetWorkField');

module.exports = {
    '/entity-create/': {
        ...entities
    },
    '/entity-checking': {
        ...getCheckingEntity
    },
    '/check-pregister/': {
        ...checkPregister
    },
    '/entity-get-info/:id': {
        ...returnEnityById
    },
    '/entity-lists/': {
        ...get_entities
    },
    '/get-entity': {
        ...get_entity
    },
    '/entity-change-status/:id': {
        ...changeStatusEntity
    },
    '/entity-update/:id': {
        ...update_entity
    },
    '/entity-lists-ddl': {
        ...listEntityOnSelect
    },
    '/entity-network/': {
        ...getNetWorkField
    }

}