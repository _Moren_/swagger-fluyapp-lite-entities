module.exports = {
    post: {
        tags: ['entities'],
        description: "Crea una entidad",
        operationId: "createEntity",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/responseBodyEntity'
                    }
                }
            }
        },
        responses: {
            'API_E_200': {
                description: 'Registro con éxito',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/entityesCreated'
                        }
                    }
                }
            },
            'API_E_403': {
                description: 'Error al registrar la entidad.'
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },

            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
            'API_E_005': {
                description: 'Ya existe una entidad con este nombre.'
            },


        }
    }
}