module.exports = {
    get: {
        tags: ['entities'],
        description: "Dehabilita una entidad (estado)",
        operationId: "listEntityOnSelect",
        responses: {
            'API_E_200': {
                description: 'Listado de Entidades',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/litstingEntityValueToKiosk'
                        }
                    }
                }
            },
            'API_E_404': {
                description: 'Algo pasó al listar las Entidades.',
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
            'API_E_005': {
                description: 'Ya existe una entidad con este nombre.'
            },


        }
    }
}