module.exports = {
    post: {
        tags: ['themes'],
        description: "Crea un tema personalizado",
        operationId: "createTheme",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/createTheme'
                    }
                }
            }
        },
        responses: {
            'API_TH_200': {
                description: 'Creación de un tema con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createTheme'
                        }
                    }
                }
            },
            'API_TH_403': {
                description: 'Falló la creación de un tema. Intente de nuevo.'
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
            'API_E_005': {
                description: 'Ya existe una entidad con este nombre.'
            },
        }
    }
}