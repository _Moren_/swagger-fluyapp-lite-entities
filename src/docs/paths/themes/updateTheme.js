module.exports = {
    put: {
        tags: ['themes'],
        description: "Actualiza un tema personalizado",
        operationId: "updateTheme",
        parameters:[
            {
                in:'params',
                name:'id'
            }
        ],
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/requestBodyThemeUpdated'
                    }
                }
            }
        },
        responses: {
            'API_TH_200': {
                description: 'Datos del tema actualizados.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/responseThemeUpdated'
                        }
                    }
                }
            },
            'API_TH_403': {
                description: 'Algo falló al hacer la actualización. Intente de nuevo.'
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
            'API_E_005': {
                description: 'Ya existe una entidad con este nombre.'
            },
        }
    }
}