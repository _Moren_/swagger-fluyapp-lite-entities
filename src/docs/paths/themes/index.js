const createTheme = require('./createTheme');
const updateTheme = require('./updateTheme');
const listsThemes = require('./listsThemes');

module.exports = {
    '/theme-create/': {
        ...createTheme
    },
    '/theme-update/:id': {
        ...updateTheme
    },
    '/theme-lists/': {
        ...listsThemes
    }
}