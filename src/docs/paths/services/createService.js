module.exports = {
    post: {
        tags: ['services'],
        description: "Crea un servicio",
        operationId: "createService",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/createService'
                    }
                }
            }
        },
        responses: {
            'API_S_200': {
                description: 'Registro del Servicio son éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createService'
                        }
                    }
                }
            },
            'API_S_404': {
                description: 'Fallo al registrar un Servicio.'
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
            'API_E_005': {
                description: 'Ya existe una entidad con este nombre.'
            },
        }
    }
}