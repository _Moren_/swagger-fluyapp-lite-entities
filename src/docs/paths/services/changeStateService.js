module.exports = {
    put: {
        tags: ['services'],
        description: "Actualiza el estado de un servicio",
        operationId: "changeStateService",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/requestBodyStatusService'
                    }
                }
            }
        },
        responses: {
            'API_S_200': {
                description: 'Estado del Servicio actualizado con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createService'
                        }
                    }
                }
            },
            'API_S_404': {
                description: 'El código de la Servicio no existe.'
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
            'API_E_005': {
                description: 'Ya existe una entidad con este nombre.'
            },
        }
    }
}