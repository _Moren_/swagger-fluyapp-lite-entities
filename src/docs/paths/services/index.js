const createService = require('./createService');
const updateService = require('./updateService');
const changeStateService = require('./changeStateService');
const getServiceByEntity = require('./getServiceByEntity')
const _getServiceByEntity = require('./_getServiceByEntity');
const listServicesOnView = require('./listServicesOnView');
const servicesToKiosko = require('./servicesToKiosko');
const getServiceByArray = require('./getServiceByArray');
const getServices = require('./getServices');

module.exports = {
    '/service-create/': {
        ...createService
    },
    '/service-update/:id': {
        ...updateService
    },
    '/service-change-state/:id': {
        ...changeStateService
    },
    '/services-by-entity/': {
        ..._getServiceByEntity
    },
    '/service-lists': {
        ...getServiceByEntity
    },
    '/service-lists-ddl/': {
        ...listServicesOnView
    },
    '/local-kiosk/': {
        ...servicesToKiosko
    },
    '/service-lists-all/': {
        ...getServiceByArray
    },
    '/service-view/:id': {
        ...getServices
    }
}