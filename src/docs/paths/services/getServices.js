module.exports = {
    get: {
        tags: ['services'],
        description: "Retorna valores de un servicio",
        operationId: "getServices",
        parameters: [
            {
                in: 'params',
                name: 'id',
            }
        ],
        responses: {
            'API_S_200': {
                description: 'Información del Servicio.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/requestBodyServiceUpdate'
                        }
                    }
                }
            },
            'API_S_403': {
                description: 'El id del Servicio  no existe.'
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
            'API_E_005': {
                description: 'Ya existe una entidad con este nombre.'
            },
        }
    }
}