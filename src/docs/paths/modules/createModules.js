module.exports = {
    post: {
        tags: ['modules'],
        description: "Verifica si el agente tiene un modulo asignado",
        operationId: "createModules",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/createModule'
                    }
                }
            }
        },
        responses: {
            'API_M_200': {
                description: 'Creación de Módulo con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createModule'
                        }
                    }
                }
            },
            'API_M_403': {
                description: 'Error al creadl el módulo.'
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
        }
    }
}