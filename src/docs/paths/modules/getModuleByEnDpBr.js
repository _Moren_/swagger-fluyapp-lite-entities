module.exports = {
    get: {
        tags: ['modules'],
        description: "Retorna ciertos valores sobre el módulo",
        operationId: "getModuleByEnDpBr",
        parameters: [
            {
                in: 'query',
                name: 'entityId'
            },
            {
                in: 'query',
                name: 'branchId'
            },
            {
                in: 'query',
                name: 'departamentId'
            },
        ],
        responses: {
            'API_M_200': {
                description: 'Información del módulo.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/responsebodyModule'
                        }
                    }
                }
            },
            'API_M_404': {
                description: 'No tiene módulo asociado.',
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
        }
    }
}