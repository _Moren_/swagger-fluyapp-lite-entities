const createModules = require('./createModules');
const modulesLists = require('./modulesLists');
const updateModule = require('./updateModule')
const getModuleByEnDpBr = require('./getModuleByEnDpBr');

module.exports = {
    '/module-create/': {
        ...createModules
    },
    '/module-lists/': {
        ...modulesLists
    },
    '/module-by-profile/': {
        ...getModuleByEnDpBr
    },
    '/module-update/:id': {
        ...updateModule
    }
}