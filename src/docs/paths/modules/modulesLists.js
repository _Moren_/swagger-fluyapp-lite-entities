module.exports = {
    get: {
        tags: ['modules'],
        description: "Retorna todos los módulos según el identificador de la entidad",
        operationId: "modulesLists",
        parameters: [
            {
                in: 'query',
                name: 'entityId'
            }
        ],
        responses: {
            'API_M_200': {
                description: 'Listado de Módulos con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createModule'
                        }
                    }
                }
            },
            'API_M_404': {
                description: 'No hay registros de Módulos.'
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
        }
    }
}