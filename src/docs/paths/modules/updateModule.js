module.exports = {
    put: {
        tags: ['modules'],
        description: "Actualiza un módulo",
        operationId: "updateModule",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/requestBodyUpdateModule'
                    }
                }
            }
        },
        responses: {
            'API_M_200': {
                description: 'Módulo actualizado con éxito',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createModule'
                        }
                    }
                }
            },
            'API_M_404': {
                description: 'El código del Módulo no existe.'
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
        }
    }
}