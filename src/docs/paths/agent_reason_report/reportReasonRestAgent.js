module.exports = {
    post: {
        tags: ['resason_rests_report'],
        description: "Reporte sobre las razones de descanso de los agentes",
        operationId: "reportReasonRestAgent",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/requestBodyAgentReasonToFind'
                    }
                }
            }
        },
        responses: {
            'API_ART_200': {
                description: 'Reigstros de descanso del agente seleccionado.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createAgentReason'
                        }
                    }
                }
            },
            'API_ART_403': {
                description: 'No tiene registros el agente seleccionado.'
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
            'API_E_005': {
                description: 'Ya existe una entidad con este nombre.'
            },
        }
    }
}