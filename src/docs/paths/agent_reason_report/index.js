const reportReasonRestAgent = require('./reportReasonRestAgent');

module.exports = {
    '/agent-lists-reason/': {
        ...reportReasonRestAgent
    }
}