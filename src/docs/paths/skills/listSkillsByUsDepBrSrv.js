module.exports = {
    get: {
        tags: ['skills'],
        description: "Retorna datos de las habilidades",
        operationId: "listSkillsByUsDepBrSrv",
        parameters: [
            {
                in:'query',
                name:'userId'
            },
            {
                in:'query',
                name:'entityId'
            },
            {
                in:'query',
                name:'branchId'
            },
            {
                in:'query',
                name:'departamentId'
            },
        ],
        responses: {
            'API_SK_200': {
                description: 'Información de sus Habilidades.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/responseSkill'
                        }
                    }
                }
            },
            'API_SK_404': {
                description: 'Este usuario no tiene habilidades asignadas.'
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
            'API_E_005': {
                description: 'Ya existe una entidad con este nombre.'
            },
        }
    }
}