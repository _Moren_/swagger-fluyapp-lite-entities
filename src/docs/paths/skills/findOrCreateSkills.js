module.exports = {
    post: {
        tags: ['skills'],
        description: "Crea una habilitad",
        operationId: "createServiceToBranches",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/createSkills'
                    }
                }
            }
        },
        responses: {
            'API_SK_200': {
                description: 'Creación de Habilidades con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createSkills'
                        }
                    }
                }
            },
            'API_SK_200_': {
                description: 'Habilidad actualziada con éxito. (se coloco un guión bajo para diferenciar las respuestas)',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createSkills'
                        }
                    }
                }
            },
            'API_SK_404': {
                description: 'Fallo en la actualización.'
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
            'API_E_005': {
                description: 'Ya existe una entidad con este nombre.'
            },
        }
    }
}