const findOrCreateSkills = require('./findOrCreateSkills');
const updateSkills = require('./updateSkills');
const skillsLists = require('./skillsLists')
const listSkillsByUsDepBrSrv = require('./listSkillsByUsDepBrSrv');
const getAgentsSkills = require('./getAgentsSkills')

module.exports = {
    '/skills-create/': {
        ...findOrCreateSkills
    },
    '/skills-user/': {
        ...listSkillsByUsDepBrSrv
    },
    '/skills-update/:id': {
        ...updateSkills
    },
    '/skills-lists/': {
        ...skillsLists
    },
    '/skills-get-agents': {
        ...getAgentsSkills
    }
}