const entities = require('./entities/index');
const branches = require('./branches/index');
const kiosks = require('./kiosks/index');
const departaments = require('./departaments/index');
const agentReasonReport = require('./agent_reason_report/index');
const agentReasons = require('./agent_reason/index');
const modulesAgents = require('./modules-agents/index');
const modules = require('./modules/index');
const reasonRest = require('./reason-rest/index');
const services = require('./services/index');
const serviceBranches = require('./service-branch/index');
const skills = require('./skills/index');
const themes = require('./themes/index');

module.exports = {
    paths: {
        ...entities,
        ...branches,
        ...departaments,
        ...agentReasonReport,
        ...agentReasons,
        ...kiosks,
        ...modulesAgents,
        ...modules,
        ...reasonRest,
        ...services,
        ...serviceBranches,
        ...skills,
        ...themes
    }
}