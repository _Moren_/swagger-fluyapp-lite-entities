module.exports = {
    put: {
        tags: ['departaments'],
        description: "actualiza los valores de un departamento",
        operationId: "update_departament",
        parameters: [
            {
                in: 'params',
                name: 'id'
            }
        ],
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/requesyBodyUpdateValueFromDepartament'
                    }
                }
            }
        },
        responses: {
            'API_D_200': {
                description: 'Departamento actualizada con éxito',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createDepartament'
                        }
                    }
                }
            },
            'API_D_404': {
                description: 'El codigo de la Departamento no existe.'
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
            'API_E_005': {
                description: 'Ya existe una entidad con este nombre.'
            },
        }
    }
}