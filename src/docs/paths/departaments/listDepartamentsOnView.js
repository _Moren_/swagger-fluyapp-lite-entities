module.exports = {
    get: {
        tags: ['departaments'],
        description: "retorna los departamentos por el identificador de a entidad que tiene asociada a la página de administración",
        operationId: "listDepartamentsOnView",
        parameters: [
            {
                in: 'query',
                name: 'entityId'
            }
        ],
        responses: {
            'API_D_200': {
                description: 'Listado de Departamentos con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/responseDepartamentList'
                        }
                    }
                }
            },
            'API_D_404': {
                description: 'No hay registros de algún Departamento'
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
            'API_E_005': {
                description: 'Ya existe una entidad con este nombre.'
            },
        }
    }
}