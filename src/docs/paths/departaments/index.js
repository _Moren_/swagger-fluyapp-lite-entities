const create_new_depart = require('./create_new_depart');
const gets_departaments = require('./gets_departaments');
const departamentToKiosko = require('./departamentToKiosko');
const departamentsByEntity = require('./departamentsByEntity');
const update_departament = require('./update_departament')
const listDepartamentsOnView = require('./listDepartamentsOnView')
const changeSatusDepartament = require('./changeSatusDepartament')

module.exports = {
    '/departament-create': {
        ...create_new_depart
    },
    '/departament-lists-modules/': {
        ...gets_departaments
    },
    '/departament-lists-ddl/': {
        ...listDepartamentsOnView
    },
    '/floor-kiosk': {
        ...departamentToKiosko
    },
    '/departament-lists': {
        ...departamentsByEntity
    },
    '/departament-update-state/': {
        ...changeSatusDepartament
    },
    '/departament-update/:id': {
        ...update_departament
    }
}