module.exports = {
    get: {
        tags: ['departaments'],
        description: "retorna los departamentos en base al identificador de la entidad",
        operationId: "departamentsByEntity",
        parameters: [
            {
                in: 'query',
                name: 'branchId'
            },
            {
                in: 'query',
                name: 'limit'
            },
            {
                in: 'query',
                name: 'page'
            },
            {
                in: 'query',
                name: 'entityId'
            },
        ],
        responses: {
            'API_D_200': {
                description: 'Listado de Departamentos con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/responseDepartamentPagination'
                        }
                    }
                }
            },
            'API_D_404': {
                description: 'No hay registros de algún Departamento.'
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
            'API_E_005': {
                description: 'Ya existe una entidad con este nombre.'
            },
        }
    }
}