module.exports = {
    get: {
        tags: ['resason_rests'],
        description: "Retorna todas las razones de descanso por entidad",
        operationId: "listReasonRest",
        parameters: [
            {
                in: 'query',
                name: 'entityId'
            },
        ],
        responses: {
            'API_R_200': {
                description: 'Listado de Razones de Descanso con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createReasonRest'
                        }
                    }
                }
            },
            'API_R_403': {
                description: 'No hay registros de las Razones de Descanso.'
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
            'API_E_005': {
                description: 'Ya existe una entidad con este nombre.'
            },
        }
    }
}