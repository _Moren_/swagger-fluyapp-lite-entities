const createReasonRest = require('./createReasonRest');
const getReasonRestByUserEntity = require('./getReasonRestByUserEntity');
const getReasonRest = require('./getReasonRest');
const listReasonRest = require('./listReasonRest');
const updateReasonRest = require('./updateReasonRest')
const changesReasonRest = require('./changesReasonRest')

module.exports = {
    '/reason-create/': {
        ...createReasonRest
    },
    '/reason-by-user/': {
        ...getReasonRestByUserEntity
    },
    '/get-reason/': {
        ...getReasonRest
    },
    '/reason-lists/': {
        ...listReasonRest
    },
    '/reason-update/:id': {
        ...updateReasonRest
    },
    '/reason-status/:id': {
        ...changesReasonRest
    }
}