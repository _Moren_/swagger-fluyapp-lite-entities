module.exports = {
    post: {
        tags: ['resason_rests'],
        description: "Crea la razón de descanso",
        operationId: "createReasonRest",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/requestBodyReasonRest'
                    }
                }
            }
        },
        responses: {
            'API_R_200': {
                description: 'Creación de Razones de Descanso con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createReasonRest'
                        }
                    }
                }
            },
            'API_R_403': {
                description: 'Fallo en crear una razón de descanso. Intente de nuevo.'
            },
            'API_E_001': {
                description: 'ERROR DE CONEXION DE BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_002': {
                description: 'SERVICIO DE BASE DE DATO DE ENTIDADES NO DISPONIBLE. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_003': {
                description: 'ERROR DE USUARIO Y CONTRASENA EN LA BASE DE DATO DE ENTIDADES. PONGASE EN CONTACTO CON EL ADMINISTRADOR'
            },
            'API_E_004': {
                description: 'Ups, algo salio mal intente otra vez.'
            },
            'API_E_005': {
                description: 'Ya existe una entidad con este nombre.'
            },
        }
    }
}